
## v0.2.0

### Added

- Pre-commit hook flutter format
- Pre-commit hook flutter analyze

## v0.1.0

### Added

- Pre-commit hook dart format
- Pre-commit hook dart analyze
- Pre-commit hook dart analyze, consider info messages
