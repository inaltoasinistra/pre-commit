Collection of **pre-commit.com** plugins

## Features

Dart language pre-commit format and analyze. More will come.

## Getting started

In order to use pre-commit hooks install the `pre-commit` software.

```bash
pip3 install pre-commit
```

After the configuration of the project (see Usage section and **pre-commit.com** documentation),
proceed with hooks installation to the git repository.

```bash
pre-commit install
```

## Usage

In order to use the provided pre-commit hooks configure this repo in the file
`.pre-commit-config.yaml` of your project and enable the desired hook plugins.

Example configuration:

```yaml
  - repo: https://gitlab.com/bcademycode/general/pre-commit.git
    rev: v0.2.0
    hooks:
      - id: dart-format
      - id: dart-analyze
      - id: dart-analyze-pedantic
      - id: flutter-format
      - id: flutter-analyze
```

It is possible to skip selected hooks using the shell variable `SKIP`. It must
be valued with hooks ids. Multiple ids must be comma separated.

```bash
SKIP=dart-format git commit

SKIP=dart-format,dart-analyze git commit
```

In order to skip pre-commit checks at all it is possible to use the git parameter `--no-verify`

```bash
git commit --no-verify
```
